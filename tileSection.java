package Stage1;

/*
 * There are 9 of these in a tile, and these help us know what is 
 * or is not on certain parts of a tile
 */

public class tileSection {
	private String SectionType;
	private boolean HasBanner;
	
	public tileSection(String input){
		SectionType = new String();
		SectionType = input;
		HasBanner = false;
	}
	
	public void assignBanner(boolean banner){
		HasBanner = banner;
	}
	
	public boolean getBanner(){
		return HasBanner;
	}
	public String getSectionType(){
		return SectionType;
	}
	
	
}
