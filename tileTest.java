package Stage1;

import static org.junit.Assert.*;

import org.junit.Test;

/*
 * this test is to test the function of placing a tile
 * */

public class tileTest {
	
	public board dashboard=new board();
	 //test

	
  // this test is to put the first tile in the board and return true naturally
	@Test 
	public void placetest01(){
			tile newtile01 =new tile("Field", "Field","Road", "Field", "Field", 
				"Field" , "Field", "Field", "Monestary");  //create a new tile
		     boolean expected=true;
		     int x= 10; int y=10;
		     dashboard.placeTile(newtile01, x, y);//check for legal placement
		     boolean actual= dashboard.getTile(x, y).equals(newtile01);//check if the tile is put in the place we assigned
		     assertTrue("the expected result is "+ expected + "but the actual result is"+ actual,actual==expected);
		   
		    tile newtile02 =new tile("Road", "Field","Field", "Field", "Road", 
					"Field" , "Road", "Field", "CrossRoad");  //create a new tile
		      expected=true;
		      x= 11;  y=10;
		      dashboard.placeTile(newtile02, x, y);//check for legal placement
		      actual= dashboard.getTile(x, y).equals(newtile02);//check if the tile is put in the place we assigned
		      assertTrue("the expected result is "+ expected + "but the actual result is"+ actual,actual==expected);
	
			tile newtile03 =new tile("Field", "Field","Field", "Field", "Field", 
						"City" , "City" , "City" , "City" ); //create a new tile
			  expected=false;
			  x= 12;  y=10;
			  dashboard.placeTile(newtile03, x, y);//check for legal placement
			  actual= dashboard.getTile(x, y)!=null;//since we know the tile can't put in the place we assigned so check if it is equal to null
			  assertTrue("the expected result is "+ expected + "but the actual result is"+ actual,actual==expected);
			   
			 //using newtile03 to put in a place where no tile is nearby 
			 expected=false;
			 x= 15;  y=16;
			 dashboard.placeTile(newtile03, x, y);//check for legal placement			 
			 actual= dashboard.getTile(x, y)!=null;//since we know the tile can't put in the place we assigned so check if it is equal to null
			 assertTrue("the expected result is "+ expected + "but the actual result is"+ actual,actual==expected);
	}
	
}

	// this test put the second tile in the right side of the first tile, it matches
/*	@Test 
	public void placetest02(){
		tile newtile02 =new tile("Road", "Field","Field", "Field", "Road", 
				"Field" , "Road", "Field", "CrossRoad");  //  create a new tile
	     boolean expected=true;
	     int x= 11; int y=10;
	     dashboard.placeTile(newtile02, x, y);
	   boolean actual= dashboard.getTile(x, y).equals(newtile02);
	   assertTrue("the expected result is "+ expected + "but the actual result is"+ actual,actual==expected);

}*/
	
	// this test put the third tile in the right side of the second tile, but they do not match, so the third tile is not put into the board
/*	@Test 
	public void placetest03(){
		 tile newtile03 =new tile("Field", "Field","Field", "Field", "Field", 
				"City" , "City" , "City" , "City" ); //  create a new tile
	     boolean expected=false;
	     int x= 12; int y=10;
	     dashboard.placeTile(newtile03, x, y);
	     
	   tile emp= dashboard.getTile(12,10);
	   boolean actual=( emp==null);
	   assertTrue("the expected result is "+ expected + "but the actual result is"+ actual,actual==expected);

}*/
	// this is test is to test the cases when a player dose not put the new tile around the old ones, it will return false
/*	@Test 
	public void placetest04(){
		 tile newtile03 =new tile("Field", "Field","Field", "Field", "Field", 
				"City" , "City" , "City" , "City" ); //  create a new tile
	     boolean expected=true;
	     dashboard.tileNum=2;
	     int x= 15; int y=16;
	      dashboard.placeTile(newtile03, x, y);
	      
	   tile emp= dashboard.getTile(15,16);
	   boolean actual=( emp==null);
	   assertTrue("the expected result is "+ expected + "but the actual result is"+ actual,actual==expected);

}*/
	
	
	

