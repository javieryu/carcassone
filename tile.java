package Stage1;

public class tile {
	/*
	 * Int values correspond to features on a given cardinal direction on the tile.
	 * 
	 * 0 = Field;
	 * 1 = City;
	 * 2 = Road; 
	 * 3 = CrossRoad;
	 * 4 = Monestary;
	 * 5 = Banner;
	 * 	 
	 */
	
	private tileSection N, NE, E, SE, S, SW, W, NW, C;	
	
	public tile(String n, String ne, String e, String se, String s, 
			String sw, String w, String nw, String c){
		 N  = new tileSection(n);
		 NE = new tileSection(ne);
		 E  = new tileSection(e);
		 SE = new tileSection(se);
		 S  = new tileSection(s);
		 SW = new tileSection(sw);
		 W  = new tileSection(w);
		 NW = new tileSection(nw);
		 C  = new tileSection(c);
	}

	public String getN(){
		return N.getSectionType();
	}
	public String getNE(){
		return NE.getSectionType();
	}
	public String getE(){
		return E.getSectionType();
	}
	public String getSE(){
		return SE.getSectionType();
	}
	public String getS(){
		return S.getSectionType();
	}
	public String getSW(){
		return SW.getSectionType();
	}
	public String getW(){
		return W.getSectionType();
	}
	public String getNW(){
		return NW.getSectionType();
	}
	public String getC(){
		return C.getSectionType();
	}
	
	public void rotate90(){
		tileSection i;
		i = N;
		N = W;
		W = S;
		S = E;
		E = i;
		i = NW;
		NW = SW;
		SW = SE;
		SE = NE;
		NE = i;	
	}
	
	public void rotate180(){
		rotate90();
		rotate90();
	}
	
	public void rotate270(){
		rotate90();
		rotate90();
		rotate90();
	}
	//test
}
